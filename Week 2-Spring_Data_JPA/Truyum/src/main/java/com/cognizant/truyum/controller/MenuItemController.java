package com.cognizant.truyum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cognizant.truyum.model.Menu;
import com.cognizant.truyum.service.MenuService;

@Controller
public class MenuItemController {

	@Autowired
	MenuService menuService;
	
	@RequestMapping(value="/menuListCustomer", method = RequestMethod.GET)
	public String menuItemListCustomer() {
		return "menuItemListCustomer";
	}
	
	@RequestMapping(value="/addMenuItem", method = RequestMethod.GET)
	public String addMenuItemDisplay(@ModelAttribute("menu") Menu menu) {
		return "addMenuItem";
	}
	
	@RequestMapping(value="/addMenuItem", method = RequestMethod.POST)
	public String addMenuItem(@ModelAttribute("menu") Menu menu) {
//		System.out.println(menu);
		menuService.addMenuItem(menu);
		return "menuItemAddSuccess";
	}
	
	@RequestMapping(value="/menuListForUser", method = RequestMethod.GET)
	public String menuListForUser(ModelMap model) {
//		for(Menu menu: menuService.listOfMenuForCustomer()) {
//			System.out.println("Menu  " + menu);
//		}
		model.put("menuList", menuService.listOfMenuForCustomer());
		return "menuItemListCustomer";
	}
	
	@RequestMapping(value="/menuListForAdmin", method = RequestMethod.GET)
	public String menuListForAdmin(ModelMap model) {
//		for(Menu menu: menuService.listOfMenuForAdmin()) {
//			System.out.println("Menu  " + menu);
//		}
		model.put("menuList", menuService.listOfMenuForAdmin());
		return "menuItemListAdmin";
	}
}
